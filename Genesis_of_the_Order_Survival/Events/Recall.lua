-----------------------------------------------------------------------------------------------------------
---------- 									   Made by Jammer                                    ----------
---------- For the latest version go to: https://gitlab.com/Jammer-gitlab/Supreme-Commander-Maps ----------
---------- 				   Do not remove this message when modifying the map				     ----------
---------- 							   For information see Readme						   	     ----------
-----------------------------------------------------------------------------------------------------------

local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local Utilities = import('/lua/utilities.lua')

local Statistics = import(ScenarioInfo.MapPath .. 'Events/Statistics.lua')

local GameSetup = import(ScenarioInfo.MapPath .. 'Functionality/GameSetup.lua')

local BroadcastMsg = import(ScenarioInfo.MapPath .. 'Utilities/BroadcastMsg.lua')
local CircularPointGenerator = import(ScenarioInfo.MapPath .. 'Utilities/CircularPointGenerator.lua')
local PingMarker = import(ScenarioInfo.MapPath .. 'Utilities/PingMarker.lua')
local PlayDialogue = import(ScenarioInfo.MapPath .. 'Utilities/PlayDialogue.lua')

local DialogueList = import(ScenarioInfo.MapPath .. 'Resources/DialogueList.lua')

-- RecallState 
-- 0 = Do nothing 
-- 1 = Next Stage 
-- 2 = Player Victory 

RecallState = 0

local EnemyArmies = GameSetup.EnemyArmies
local PlayerArmies = GameSetup.PlayerArmies

local InsideRecall = { }

local FirstRecallDialog = true

-- RecallNumber to chose which thread should start
-- 0 = Recall
-- 1 = Endless recall

function StartRecall(RecallNumber)
    -- Reset Value's
    RecallStop = 0
    RecallState = 0

    local RecallPosition = ScenarioUtils.MarkerToPosition('recallposition-1')
    local RecallDistance = 12
    local RecallBeacons = 16

    --Spanws beacons
    SpawnBeaconsThread(RecallPosition, RecallDistance, RecallBeacons, RecallNumber)
    -- Which Commanders are alive and where
    ForkThread(PlayerLocationThread, RecallPosition, RecallDistance, PlayerArmies)
    -- Recall
    ForkThread(RecallThread, PlayerArmies, RecallNumber) 

end

function SpawnBeaconsThread(RecallPosition, RecallDistance, RecallBeacons, RecallNumber)

    local DialogueCount = 0

    local Army = "ARMY_ALLY_UEF"
    local ArmyNumber = table.getn(ListArmies()) - 4
    local UnitTable = {'ueb5103'}
    local Count = 0
    local MaxCount = 20
    local CenterUnit = CreateUnitHPR('ueb5102', Army, RecallPosition[1], RecallPosition[2], RecallPosition[3], 0, 0, 0)
    CenterUnit.CanBeKilled = false

    
    local DecalTexture = (ScenarioInfo.MapPath .. '/Resources/textures/markers/beacon-quantum-gate_btn_up.dds')
    if RecallNumber == 0 then
        CreateDecal(RecallPosition, 0, DecalTexture, '', 'Albedo', 25, 25, 10000, 70, Army, 0) 
    end    
    if RecallNumber == 1 then
        CreateDecal(RecallPosition, 0, DecalTexture, '', 'Albedo', 25, 25, 10000, 0, Army, 0) 
    end

    PingMarker.getAlertMarkerLong(RecallPosition, ArmyNumber)

    ForkThread(
        function()
            while true do 

                -- make beacons over and over for visual effect
                if Count <= MaxCount then
                    local Beacons = CircularPointGenerator.CircularSpawner(UnitTable, Army, RecallBeacons, RecallDistance, RecallPosition)
                end
              
                local Beacons = GetArmyBrain(Army):GetListOfUnits(categories.ueb5103 , false, false)

                for i, Beacon in Beacons do
                    Beacon:SetDoNotTarget(true)
                    Beacon.CanBeKilled = false
                end  

                -- visual for center of recall
                if RecallState == 0 then
                    if not CenterUnit.Dead then
                        CenterUnit:CreateProjectile('/effects/entities/UnitTeleport01/UnitTeleport01_proj.bp', 0, 1.35, 0, nil, nil, nil):SetCollision(false)
                        WaitSeconds(1.5)
                        CenterUnit:CreateProjectile('/effects/entities/UnitTeleport01/UnitTeleport01_proj.bp', 0, 1.35, 0, nil, nil, nil):SetCollision(false)

                    end
                end

                -- if recallstop then kill all
                if RecallStop == 1 then
                    if RecallNumber == 0 then
                        CenterUnit.CanBeKilled = true
                        for i, Beacon in Beacons do
                            Beacon:SetDoNotTarget(false)
                            Beacon.CanBeKilled = true
                        end    
                        WaitSeconds(1)
                        -- kill all units
                        IssueDestroySelf(Beacons)
                        IssueDestroySelf(CenterUnit)
                        -- spawn 1 more beacon 
                        local Beacons = CircularPointGenerator.CircularSpawner(UnitTable, Army, RecallBeacons, RecallDistance, RecallPosition)
                        WaitSeconds(4)
                        local Beacons = GetArmyBrain(Army):GetListOfUnits(categories.ueb5103 , false, false)

           
                        
                        IssueKillSelf(Beacons)
                        if DialogueCount == 0 then
                            if RecallState < 2 then
                                PlayDialogue.Dialogue(DialogueList.Astrox_4, nil, false) --Astrox Hall: Do whatever it takes to survive, Commander. We can't afford to lose you, too. Hall out.
                            end
                        end
                        DialogueCount = DialogueCount + 1 
                        break
                    end
                end


                Count = Count + 1
                
                -- if beacon count is higher then max then destroy
                if Count == MaxCount then
                    IssueDestroySelf(Beacons)
                    -- spawn 1 more beacon 
                    local Beacons = CircularPointGenerator.CircularSpawner(UnitTable, Army, RecallBeacons, RecallDistance, RecallPosition)
                end

                
                if RecallStop == 1 then 
                    break
                end
                WaitSeconds(5)
            end
        end)
end

function RecallThread(PlayerArmies, RecallNumber)
    local AllPlayersInsideRecall = false
    local RecallTimer = 10
    local CountDownTimer = 60
    
    if FirstRecallDialog == false then--[[All Recall Dialog]]--
        Dialog = {
            {displayTime = 90, text =
            "All Main Commanders to Recall Zone.\n\n\n\nThis is a OPTIONAL recall.\n Next recall is in 10 minutes",}
        }
    end    
    if FirstRecallDialog == true then --[[First time Dialog]]--
        Dialog = {
            {displayTime = 90, text =
            "All Main Commanders to Recall Zone. \nSeraphim invasion is imminent!!!\n\n\nThis is a OPTIONAL recall.\n Next recall is in 10 minutes",}
        }
        FirstRecallDialog = false
    end


    BroadcastMsg.DisplayDialogBox("right", Dialog, false)
    BroadcastMsg.TextMsg(string.rep(" ", 29) .. "Game will continue in: ", 45, 'e80a0a', 75, 'lefttop')

    if RecallNumber == 0 then

    end

    while true do 
        -- All Commanders inside recallarea start recall timer
        if CheckAll(InsideRecall) == true then
            if RecallTimer >= 0 then
                BroadcastMsg.TextMsg("Recall in: " .. RecallTimer .. string.rep(" ", 200), 30, 'e80a0a', 0.29, 'center')
            end
            RecallTimer = RecallTimer - 1
            if CountDownTimer == 0 then
                CountDownTimer = CountDownTimer + 1
            end
        -- When false reset recall timer    
        else
            RecallTimer = 10
        end

        -- If timer = 0 then Players win
        if RecallTimer == - 1 then
            
            VictoryWooopWooop()             
            
            -- Update Status to Player Victory
            RecallStop = 1
            RecallState = 2
            break
        end

        -- Text and Timer for Optional Recall
        if RecallNumber == 0 then
            BroadcastMsg.TextMsg(string.rep(" ", 45) , 20, 'e80a0a', 0.5, 'lefttop')
            BroadcastMsg.TextMsg(string.rep(" ", 40) .. "" .. CountDownTimer, 50, 'e80a0a', 0.20, 'lefttop')

            -- If timer = 0 then Stop Recall
            if CountDownTimer <= 0 then
                RecallStop = 1 
                -- Update Status to Next Stage
                RecallState = 1
                NextRecallTimer()
                break
            end

            CountDownTimer = CountDownTimer - 1
        end
        
        WaitSeconds(1.2)
    end
end

function PlayerLocationThread(RecallPosition, RecallDistance, PlayerArmies)
    while true do 
        if RecallStop == 1 then 
            break
        end
        for i, Army in PlayerArmies do
            -- get commanders from player armies
            local Commander = GetArmyBrain(Army):GetListOfUnits(categories.COMMAND , false, false)[1]
            -- if he has commander then 
            if Commander then 
                local Distance = VDist3(Commander:GetPosition(),RecallPosition)	
                if InsideRecallArea(Commander, Distance, RecallDistance) then
                    InsideRecall[i] = {Commander.UnitId, true}   
                else 
                    InsideRecall[i] = {Commander.UnitId, false}
                end
            end
            -- if commander is dead
            if not Commander then 
                InsideRecall[i] = nil
            end
        end
        WaitSeconds(2)
    end
end

function InsideRecallArea(Commander, Distance, RecallDistance)
    -- if inside return true
    if Distance <= RecallDistance then 
        return true
    end
    -- not inside return false
    if Distance >= RecallDistance then 
        return false
    end
end


function CheckAll(Table)
    -- Check if all commanders are inside recall area
    for _, Commander in Table do
        if not Commander[2] then
            return 
        end
    end
    -- if all true then return true
    return true
end

function VictoryWooopWooop()
    ForkThread(function()
        PlayDialogue.Dialogue(DialogueList.Entropy_10, nil, false) --Entropy: We've got a lock on your ACU. Almost there.
        for i, Army in PlayerArmies do
            -- Teleport Commanders

            local Commander = GetArmyBrain(Army):GetListOfUnits(categories.COMMAND , false, false)[1]
            if Commander then
                ScenarioFramework.FakeTeleportUnit(Commander, true)
            end
        end
        PlayDialogue.Dialogue(DialogueList.Entropy_11, nil, false) --Entropy: That was damn close, Commander. Glad we got you out of there in one piece.
        WaitSeconds(6)
        for i, Army in PlayerArmies do
            -- Score should be visable now
            GetArmyBrain(Army):OnVictory()

            Statistics.Setup()
            WaitSeconds(4)
            EndGame()
        end
        for i, Army in EnemyArmies do 
            GetArmyBrain(Army):OnDefeat()
        end 
    end)
end

function NextRecallTimer()
    local NextRecallTimer = 595
    ForkThread(function()
        WaitSeconds(5)
        BroadcastMsg.TextMsg(string.rep(" ", 90) .. "Next Recall in: ", 25, 'e80a0a', 595, 'lefttop')
        while true do 
            if NextRecallTimer > 59 then
                local Minutes = math.round((NextRecallTimer/60) - 0.5) 
                local TotalSecondsMinutes = Minutes * 60
                local Seconds = NextRecallTimer - TotalSecondsMinutes
                if Seconds < 10 then
                    Seconds = "0"..Seconds
                end
                Time = Minutes..":"..Seconds
            end
            if NextRecallTimer < 60 then
                Time = NextRecallTimer
            end

            BroadcastMsg.TextMsg(string.rep(" ", 97) .. "" .. Time, 25, 'e80a0a', 0.00001, 'lefttop')
            NextRecallTimer = NextRecallTimer - 1
            if NextRecallTimer < 0 then 
                break
            end
            WaitSeconds(1)        
        end
    end)
end