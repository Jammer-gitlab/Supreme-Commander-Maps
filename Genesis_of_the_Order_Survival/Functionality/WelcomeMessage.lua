-----------------------------------------------------------------------------------------------------------
---------- 									   Made by Jammer                                    ----------
---------- For the latest version go to: https://gitlab.com/Jammer-gitlab/Supreme-Commander-Maps ----------
---------- 				   Do not remove this message when modifying the map				     ----------
---------- 							   For information see Readme						   	     ----------
-----------------------------------------------------------------------------------------------------------

local BroadcastMsg = import(ScenarioInfo.MapPath .. 'Utilities/BroadcastMsg.lua')



function ShowWelcomeMessage()
	
	local Difficulty = nil
	if ScenarioInfo.Options.Option_PlatoonWaveCount == 1 then
		Difficulty = "Easy"
	elseif ScenarioInfo.Options.Option_PlatoonWaveCount == 2 then
		Difficulty = "Moderate"
	elseif ScenarioInfo.Options.Option_PlatoonWaveCount == 3 then
		Difficulty = "Hard"
	end
	ForkThread(function()
		WaitSeconds(1)
		BroadcastMsg.TextMsg(string.rep(" ", 50) .. "", 20, '2F4607', 20, 'lefttop')
		BroadcastMsg.TextMsg(string.rep(" ", 50) .. "", 20, '2F4607', 20, 'lefttop')
		BroadcastMsg.TextMsg(string.rep(" ", 50) .. "", 20, '2F4607', 20, 'lefttop')
		BroadcastMsg.TextMsg(string.rep(" ", 50) .. "", 20, '2F4607', 20, 'lefttop')
		BroadcastMsg.TextMsg(string.rep(" ", 30) .. "Genesis of the Order ", 36, '77F550', 20, 'lefttop')
		BroadcastMsg.TextMsg(string.rep(" ", 75) .. "Version " .. ScenarioInfo.map_version, 25, '77F550', 20, 'lefttop')

		BroadcastMsg.TextMsg(string.rep(" ", 50) .. "Difficulty " .. Difficulty, 20, '77F550', 20, 'lefttop')
		BroadcastMsg.TextMsg(string.rep(" ", 50) .. "Enemy health " .. (ScenarioInfo.Options.Option_HealthMultiplier * 100) .. "%", 20, '77F550', 20, 'lefttop')
		BroadcastMsg.TextMsg(string.rep(" ", 50) .. "Enemy damage " .. (ScenarioInfo.Options.Option_DamageMultiplier * 100) .. "%", 20, '77F550', 20, 'lefttop')
	    BroadcastMsg.TextMsg(string.rep(" ", 50) .. "Enemies spawn in " .. ScenarioInfo.Options.Option_BuildTime .. " seconds", 20, '77F550', 20, 'lefttop')

	end)
end

function ShowBriefing()
	ForkThread(function()
		WaitSeconds(1)
		BroadcastMsg.TextMsg(string.rep(" ", 5) .. "Commander, this objective is of utmost importance.", 											   	18, 'FFFFFF', 35, 'leftcenter')
		BroadcastMsg.TextMsg(string.rep(" ", 5) .. "We recieved intel about an Order project code-named: Little Behemoth.", 						   	18, 'FFFFFF', 35, 'leftcenter')
		BroadcastMsg.TextMsg(string.rep(" ", 5) .. "It seems the Order of the Illuminate are working on a new kind of Experimental." , 					18, 'FFFFFF', 35, 'leftcenter')
		BroadcastMsg.TextMsg(string.rep(" ", 5) .. "Admiral Jip's mission was to locate and destroy it.", 										   	   	18, 'FFFFFF', 35, 'leftcenter')
		BroadcastMsg.TextMsg(string.rep(" ", 5) .. "During the search he got overrun by the Order and is in need of assistance.", 					   	18, 'FFFFFF', 35, 'leftcenter')
		BroadcastMsg.TextMsg(string.rep(" ", 5) .. "He got corned and had to sail his Summit onto the coast, protect it at all cost.", 			   		18, 'FFFFFF', 35, 'leftcenter')
		BroadcastMsg.TextMsg(string.rep(" ", 5) .. "The Order is still in the area and are preparing another attack.", 							       	18, 'FFFFFF', 35, 'leftcenter')
		BroadcastMsg.TextMsg(string.rep(" ", 5) .. "Your objective is to hold off all enemy attacks." , 											   	18, 'FFFFFF', 35, 'leftcenter')
		BroadcastMsg.TextMsg(string.rep(" ", 5) .. "On top of that, our long range radar is picking up Seraphim movement in Order territory.", 	       	18, 'FFFFFF', 35, 'leftcenter')
		BroadcastMsg.TextMsg(string.rep(" ", 5) .. "Be on your guard. Be courageous. HQ out!", 													   		18, 'FFFFFF', 35, 'leftcenter')
	end)
end
