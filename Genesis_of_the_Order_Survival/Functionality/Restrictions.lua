-----------------------------------------------------------------------------------------------------------
---------- 									   Made by Jammer                                    ----------
---------- For the latest version go to: https://gitlab.com/Jammer-gitlab/Supreme-Commander-Maps ----------
---------- 				   Do not remove this message when modifying the map				     ----------
---------- 							   For information see Readme						   	     ----------
-----------------------------------------------------------------------------------------------------------

local ScenarioFramework = import('/lua/ScenarioFramework.lua')

	-- category operators
	-- + = or 
	-- - = subtracting
	-- * = and 
	-- / = ?

function SetupBuildRestrictions(PlayerArmies)
	for i, Army in PlayerArmies do

		ScenarioFramework.AddRestriction(Army, categories.WALL)
		ScenarioFramework.AddRestriction(Army, (categories.SCOUT * categories.LAND))
		--ScenarioFramework.AddRestriction(Army, (categories.MASSFABRICATION * categories.TECH3) - categories.SUBCOMMANDER)
		--ScenarioFramework.RemoveBuildRestriction(Army, categories.uaa0101)	

		if ScenarioInfo.Options.Option_GameBreaker == 0 then
			ScenarioFramework.AddRestriction(Army, categories.xab1401) --Paragon	
		elseif ScenarioInfo.Options.Option_GameBreaker == 1 then
			ScenarioFramework.AddRestriction(Army, categories.xsb2401) --Yolona Oss	
		elseif ScenarioInfo.Options.Option_GameBreaker == 2 then			
			ScenarioFramework.AddRestriction(Army, categories.xab1401) --Paragon
			ScenarioFramework.AddRestriction(Army, categories.xsb2401) --Yolona Oss
		elseif ScenarioInfo.Options.Option_GameBreaker == 3 then			

		end
	end
end